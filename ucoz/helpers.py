import os, time, re

#------------------------------------------------------------------------------#

if (os.name=='posix'):
    s = u'/'
elif (os.name=='nt'):
    s = u'\\'
else:
    print ('unknown platform')
    quit()

#------------------------------------------------------------------------------#

def ts(text):
    if (text=='loads'):
        return ('loads')
    elif (text=="loads_sections"):
        return ('ld_ld')

#------------------------------------------------------------------------------#

def idmod(text):
    if (text=='loads'):
        return ('2')

#------------------------------------------------------------------------------#

def mkdir(path):
    if (os.path.exists(path) != True):
        os.mkdir(path)

#------------------------------------------------------------------------------#

def rm(path):
    if (os.path.exists(path) == True):
        os.remove(path)

#------------------------------------------------------------------------------#

# перевод времени из юникстайма в формат Atom-M
def date(n):
    return str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(n)) ))

#------------------------------------------------------------------------------#

# Разбирает выбранный файл на массив
def parse(path):
    f = open(path, 'r')

    i=0
    mass = []
    while True:
        try:
            line = (f.readline())
            if (len(line) == 0):
                break
            line = line.replace("\\|", "&#124;")
            line = line.rstrip()

            if (len(mass) < i+1):
                # новый материал
                mass.append(line)
            else:
                # продолжение материала
                # удаляем слэш и добавляем новую строку
                mass[i] = (mass[i])[:-1]
                mass[i] += line

            if (line[-1] != '\\'):
                mass[i] = mass[i].split('|')
                i+=1
        except UnicodeDecodeError:
            # некоторые строки содержат какие-то нечитаемые символы
            # не нашел способа лучше, чем просто игнорировать эти строки
            pass
    return (mass)

#------------------------------------------------------------------------------#

# возвращает массив из прикреплённых картинок
def parceAttach(files):
    files = files.split("&#124;")
    ret=[]
    for attach in files:
        attach = attach.split('`')
        try:
            ret.append(attach[0]+'.'+attach[1])
        except:
            pass
    return ret

#------------------------------------------------------------------------------#

# экранирует запрещённые символы
def h(s):
    s = s.replace('<', '&lt;')
    s = s.replace('>', '&gt;')
    s = s.replace("'", '&lsquo;')
    s = s.replace('&nbsp;', ' ')
    s = s.replace('&copy;', '©')
    s = s.replace('&reg;', '®')
    s = s.replace('&trade;', '™')
    s = s.replace("&#124;", '|')
    return (s)

#------------------------------------------------------------------------------#

# парсит html в bb код
def toBB(s):
    s = re.sub('<(|\D{1})b(| [^>]*)>', r'[\1b]', s)
    s = re.sub('<br(| [^>]*)>', '\n', s)
    s = re.sub('<a href="([^"]*)" target="_blank">([^<]*)</a>', r'[url=\1]\2[/url]', s)

    while (re.search('<(font|span) color="(#[0-9a-fA-F]{6}|[0-9a-fA-F]{6}|\D*)">([^<]*)</(font|span)>', s) != None):
        s = re.sub('<(font|span) color="(#[0-9a-fA-F]{6})">([^<]*)</(font|span)>', r'[color=\2]\3[/color]', s)
        s = re.sub('<(font|span) color="([0-9a-fA-F]{6})">([^<]*)</(font|span)>', r'[color=#\2]\3[/color]', s)
        s = re.sub('<(font|span) color="(\D*)">([^<]*)</(font|span)>', r'[color=\2]\3[/color]', s)

    while (re.search('<span style="color:(#[0-9a-fA-F]{6}|\D*)">([^<]*)</span>', s) != None):
        s = re.sub('<span style="color:(#[0-9a-fA-F]{6}|\D*)">([^<]*)</span>', r'[color=\1]\2[/color]', s)

    while (re.search('<strong(| [^>]*)>([^<]*)</strong>', s) != None):
        s = re.sub('<strong(| [^>]*)>([^<]*)</strong>', r'[b]\2[/b]', s)

    while (re.search('<div align="(left|center|right)">([^<]*)</div>', s) != None):
        s = re.sub('<div align="(left|center|right)">([^<]*)</div>', r'[\1]\2[/\1]', s)

    while (re.search('<p(| [^>]*)>([^<]*)</p>', s) != None):
        s = re.sub('<p(| [^>]*)>([^<]*)</p>', r'\2', s)

    s = re.sub('<img alt=("([^"]*)") src="([^"]*)">', r'[img]\3[/img]', s)

    # неопознанные теги удаляем
    while (re.search('<([^>]*)>', s) != None):
        s = re.sub('<([^>]*)>', '', s)
    while (re.search('</([^>]*)>', s) != None):
        s = re.sub('</([^>]*)>', '', s)

    s = h(s)

    return (s)

#------------------------------------------------------------------------------#