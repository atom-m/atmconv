#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, shutil
from helpers import *
from config import *

cfg = config()


class Ucoz:
    def __init__(self):
        self.sql = {}

        # Прерывать работу, если по какой-то причине
        # не выбраны модули или передана строка
        if (isinstance(cfg.get('modules'), list) == False) or (len(cfg.get('modules')) == 0):
            print ('Ошибка выбора модулей')
            return

        # Прерывать работу, если переданы невыполнимые параметры
        self.checkParam()

        for module in cfg.get('modules'):
            print ('Load "'+ts(module)+'.txt"')
            if (os.path.exists(cfg.get('conv_in')+'_s1'+s+ts(module)+'.txt')):
                if (module == 'loads'):
                    self.parseLoads()
            else:
                print ('WARNING: File "'+ts(module)+'.txt" not found.')

        self.saveSql()

#------------------------------------------------------------------------------#

    def checkParam(self):
        errors = ''
        if (cfg.get('multicategoties') == True
            and cfg.get('atm_version') < 4):

            errors += ('Для поддержки "multicategoties" требуется '
                '"atm_version" более или равно 4\r\n')

        if (errors != ''):
            print (errors)
            quit()

#------------------------------------------------------------------------------#

    def makeSql(self, data):
        sql = ''
        if (data['type']=='insert'):
            sql = "INSERT INTO `"+data['table']+"` ("
            b=False
            for value in sorted(data['values'].keys()):
                if (b == True):
                    sql += ", "
                else:
                    b = True
                sql += "`"+str(value)+"`"
            sql += ") VALUES ("
            b=False
            for value in sorted(data['values'].keys()):
                if (b == True):
                    sql += ", "
                else:
                    b = True
                sql += "'"+str(data['values'][value])+"'"
            sql += ");"

        if (sql != ''):
            if (data['table'] not in self.sql):
                self.sql[data['table']] = ''
            self.sql[data['table']] += sql+'\r\n'

#------------------------------------------------------------------------------#

    def saveSql(self):
        if (cfg.get('glue_sql') == True):
            fname = cfg.get('conv_out')+'result.sql'
            rm(fname)

        for module in sorted(self.sql.keys()):
            if (cfg.get('glue_sql') != True):
                fname = cfg.get('conv_out')+module+'.sql'
                rm(fname)
            f = open(fname, 'a')
            sql = "DELETE FROM `"+module+"`;"+'\r\n\r\n'+self.sql[module]
            f.write(sql)
            f.close()

#------------------------------------------------------------------------------#

    # ///   ATTACHES   ///
    def parseAttaches(self, files, mat):
        if (len(files) != 0):
            i=1
            for item in files:

                # не считать размер файла, если приказано их не переносить
                if (cfg.get('nofiles') == False):
                    fnewpath = cfg.get('conv_out')+'files'+s+mat['table']+s+item
                    size = os.path.getsize(fnewpath)
                else:
                    size = 0

                data = {
                    "type": 'insert',
                    "table": mat['table']+'_attaches',
                    "values": getItems('_attaches', {"filename": item, "mat": mat['values'], "size": size, "i": i})
                }
                self.makeSql(data)
                i+=1

#------------------------------------------------------------------------------#

    # ///   MULTICATEGORIES   ///
    def parseMulticategories(self, module):
        path = cfg.get('conv_in')+'_s1'+s+'ocats.txt'
        mass = parse(path)
        out = {}

        if (len(mass) != 0):
            for item in mass:
                try:
                    if (item[0] == str(idmod(module))):
                        if (out.get(item[2]) == None):
                            out[item[2]] = item[1]
                        else:
                            out[item[2]] += ','+item[1]

                except IndexError:
                    # если по какой-либо причине получена не вся строка
                    pass
        return out

#------------------------------------------------------------------------------#

    # ///   SECTIONS   ///
    def parseSections(self, module):
        path = cfg.get('conv_in')+'_s1'+s+ts(module+'_sections')+'.txt'
        mass = parse(path)

        if (len(mass) != 0):
            for item in mass:
                try:
                    data = {
                        "type": 'insert',
                        "table": module+'_sections',
                        "values": getItems('_sections', {"item": item})
                    }
                    self.makeSql(data)

                except IndexError:
                    # если по какой-либо причине получена не вся строка
                    pass

#------------------------------------------------------------------------------#

    # ///   LOADS   ///
    def parseLoads(self):
        module = 'loads'
        path = cfg.get('conv_in')+'_s1'+s+ts(module)+'.txt'
        mass = parse(path)

        if (len(mass) != 0):
            if (cfg.get('multicategoties') == True):
                cats = self.parseMulticategories(module)

            i=1
            for item in mass:
                i+=1
                try:

                    if (cfg.get('multicategoties') == True):
                        cat = cats.get(item[0])
                    else:
                        cat = item[3]

                    data = {
                        "type": 'insert',
                        "table": module,
                        "values": getItems('loads', {"item": item, "cat": cat})
                    }
                    self.makeSql(data)

                    half_id = str(int( int(data['values']['id'])/100 ))
                    # перенос прикреплённых файлов
                    if (data['values']['download'] != '' and cfg.get('nofiles') == False):
                        infile = cfg.get('conv_in')+'_ld'+s+half_id+s+data['values']['download']
                        if (os.path.exists(infile)):
                            print ('copy "'+infile+'" file')
                            mkdir(cfg.get('conv_out')+'files'+s)
                            mkdir(cfg.get('conv_out')+'files'+s+module+s)
                            shutil.copy(infile, cfg.get('conv_out')+'files'+s+module+s)
                        else:
                            print ('file "'+data['values']['download']+'" are not found')
                        print()

                    # перенос прикреплённых картинок
                    if (item[36] != ''):
                        attaches = parceAttach(item[37])
                        at = attaches[:]
                        if (cfg.get('nofiles') == False):
                            for attach in attaches:
                                infile = cfg.get('conv_in')+'_ld'+s+half_id+s+attach
                                if (os.path.exists(infile)):
                                    print ('copy "'+infile+'" attach')
                                    mkdir(cfg.get('conv_out')+'files'+s)
                                    mkdir(cfg.get('conv_out')+'files'+s+module+s)
                                    shutil.copy(infile, cfg.get('conv_out')+'files'+s+module+s)
                                else:
                                    print('exterminate '+attach)
                                    at.remove(attach)
                            print()
                        self.parseAttaches(at, data)

                    print('Конвертация материала номер '+str(i))

                except IndexError:
                    # если по какой-либо причине получена не вся строка
                    pass
            self.parseSections(module)
            print('Всего файлов: '+str(i))



Ucoz()