from helpers import *

def config():
    return {

        # [list] список модулей, для которых нужно сделать конвертацию
        "modules": ['loads'],

        # [string] путь к папке с бэкапом юкоза
        "conv_in": '../backup/input/',

        # [string] путь к папке, где будет созданы файлы для переноса на сайт
        "conv_out": '../backup/output/',

        # [boolean] [False] если True то конвертирует не имея доступа к файловому архиву
        "nofiles": False,

        # [boolean] [False] если True то список SQL запросов будет не разделён по модулям, а в одном файле
        "glue_sql": False,

        # [boolean] [False] если True то поддерживается множественный выбор категорий
        "multicategoties": False,

        # [integer] [4] для какой версии Atom-M создавать бекаб
        "atm_version": 4

    }



def getItems(table, p):

    if (table == 'loads'):
        item = p.get('item')
        return {
            "id": item[0],
            "title": h(item[15]),
            "main": toBB(item[32]),
            "author_id": item[35],
            "category_id": p.get('cat'),
            "views": item[13],
            "downloads": item[14],
            "rate": '',
            "download": str(item[0])+'_'+str(item[24]),
            "filename": item[24],
            "download_url": '',
            "download_url_size": '',
            "date": date(item[5]),
            "comments": item[8],
            "tags": '',
            "description": toBB(item[16]),
            "sourse": item[27],
            "sourse_email": item[28],
            "sourse_site": item[29],
            "commented": item[7],
            "available": 1,
            "view_on_home": 1,
            "on_home_top": item[4],
            "premoder": 'confirmed'
        }
    elif (table == '_attaches'):
        mat = p.get('mat')
        return {
            "entity_id": mat['id'],
            "user_id": mat['author_id'],
            "attach_number": p.get('i'),
            "filename": p.get('item'),
            "size": p.get('size'),
            "date": date(0),
            "is_image": 1
        }
    elif (table == '_sections'):
        item = p.get('item')
        return {
            "id": item[0],
            "parent_id": item[1],
            "announce": '',
            "title": item[5],
            "view_on_home": 1,
            "no_access": ''
        }